#!/usr/bin/env python3
import sys

from datetime import datetime
from multiprocessing import Pool

import requests
import os
import re
import xml.etree.ElementTree as ET

sys.path.append(".")

from blueprints_ci import (  # noqa
    logger,
    Settings,
    run_cmd,
    submit_to_tuxsuite,
    get_local_manifest_xml_from_trs_manifest,
    generate_tuxsuite_repo_plan,
    print_tuxsuite_build_log,
)


#
#   Required environment variables
#
required_vars = [
    # Variables coming from ci scripts (meta-ts.yml, meta-ewaol-machine.yml, ...)
    "GIT_URL",

    # URL for default.xml, TRS's manifest file
    "TRS_MANIFEST_URL",

    # The generated token that allows pushing new branches to TRS_MANIFEST repository
    "TRS_MANIFEST_BOT_USER_TOKEN",

    # The bot user name to show up in automated commits
    "TRS_MANIFEST_BOT_USER_NAME",

    # Basic configuration to send build requests to tuxsuite.com
    "TUXSUITE_GROUP",
    "TUXSUITE_PROJECT",
    "TUXSUITE_TOKEN",

    # Variables needed to generate a Tuxsuite plan
    "TEMPLATECONF",
    "EXTRACONFIGS",
    "TARGET_TYPE",
    "TARGET",
]

DEFAULT_XML_FILENAME = "default.xml"

# FIXME: settings should be shared among threads, but there is some weird
# Python serialization issue that does not allow settings to be used with thread Pool.
__settings = None


def run_git(cmd):
    """
        Run git and raise if anything bad happens
    """
    proc = run_cmd(["git"] + cmd)
    if not proc.ok:
        output = f"stdout: {proc.out}\n"
        output += f"stderr: {proc.err}"
        raise Exception(f"Failed to run `git {' '.join(cmd)}`: {output}")
    return proc.out


def check_project_changes(params):
    """
        Returns the new commit hash, if it's different than `revision`,
        or False if it's the same or None if anything bad happens.

        If anything bad happens while trying to retrieve info from a git repository,
        the function will return None and the project will be ignored.
    """
    project, remotes = params
    if project.attrib.get("upstream") is None:
        logger.info(f"No \"upstream\" for \"{project.attrib['name']}\", will not check for changes on this one")
        return (project, False)

    revision = project.attrib["revision"]
    upstream = project.attrib["upstream"]
    remote = remotes[project.attrib.get("remote")]
    path = project.attrib["name"]
    git_url = f"{remote.attrib['fetch']}/{path}"

    try:
        logger.info(f"Checking for changes in {git_url} (revision \"{revision}\")")
        output = run_git(["ls-remote", git_url])
    except Exception as e:
        logger.warning(e)
        return (project, None)

    new_revision = None
    for line in output.split("\n"):
        if line.endswith(upstream):
            new_revision = line.split()[0]

    if new_revision is None:
        logger.warning(f"\"{git_url}\" does not contain ref \"{upstream}\": {output}")
        return (project, None)

    result = new_revision if new_revision != revision else False
    if result:
        logger.info(f"Found new commit for {git_url}: \"{revision}\" -> \"{new_revision}\"")
    return (project, result)


def build_individual_project(params):
    """
        Generate a local manifest changing only project's revision,
        then trigger a build on TuxSuite and return True or False
    """
    if len(params) == 3:
        settings, project, new_revision = params
    else:
        settings = __settings
        project, new_revision = params

    # There will be parallel submissions to tuxsuite, thus multiple files with same name
    # an easy way to work around that is to work each build on its own directory
    base = project.attrib["name"].replace(".git", "").replace("/", "-").lower()
    logger.info(f"Triggering build for {base}")
    os.makedirs(base, exist_ok=True)
    os.chdir(base)

    git_url = {
        f"/{project.attrib['name']}": new_revision,
    }
    local_manifest_xml_filename = get_local_manifest_xml_from_trs_manifest(settings, git_url)

    logger.info(f"{base}/{local_manifest_xml_filename}:")
    with open(local_manifest_xml_filename, "r") as fp:
        logger.info(fp.read())

    tuxsuite_plan = generate_tuxsuite_repo_plan(settings)

    logger.info("Submitting plan to tuxsuite")
    result = submit_to_tuxsuite(settings, tuxsuite_plan)

    os.chdir("..")

    if not result:
        print_tuxsuite_build_log(settings)

    return (project, new_revision, settings.TUXSUITE_DOWNLOAD_URL, result)


def create_manifest_mr(settings, good_revisions, bad_revisions, default_xml):
    """
        1. Git checkout a new branch. Ex: tip-{dd-mm-yyyy}
        2. Change all projects within `good_revisions`
        3. Write to default.xml
        4. Commit it
        5. Push the branch
        6. Create a merge request
    """

    if len(good_revisions) == 0:
        return False

    today_str = datetime.now().strftime("%d-%m-%Y")
    branch_name = f"track-master-{today_str}"

    # In order to CI to be able to push a branch to the manifest repository
    # we need an authenticated git url, which contains a user and token
    # Gitlab is amazing and it automagically creates a bot user for projects
    # once you create an authorization token. The bot user name is very simple
    # Ref: https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#bot-users-for-projects
    git_user = f"project_{settings.CI_PROJECT_ID}_bot"
    git_url = f"https://{git_user}:{settings.TRS_MANIFEST_BOT_USER_TOKEN}@{settings.CI_SERVER_HOST}/{settings.CI_PROJECT_PATH}.git"

    # Upgrade all projects in the manifest that have good builds
    with open(DEFAULT_XML_FILENAME, "r") as fp:
        default_xml_contents = fp.read()

    for project, new_revision in good_revisions:
        project_name = project.attrib["name"]
        current_revision = project.attrib["revision"]
        pattern = f'(<project[^>]*? name="{project_name}"[^>]*?) revision="{current_revision}" ([^>]*?>)'
        replacement = f'\\1 revision="{new_revision}" \\2'
        default_xml_contents = re.sub(pattern, replacement, default_xml_contents)

    with open(DEFAULT_XML_FILENAME, "w") as fp:
        fp.write(default_xml_contents)

    config_user = [
        "-c", f"user.email='{git_user}@{settings.CI_SERVER_HOST}'",
        "-c", f"user.name='{settings.TRS_MANIFEST_BOT_USER_NAME}'",
    ]

    try:
        run_git(["checkout", "-b", branch_name])
        run_git(config_user + ["commit", "-m", f"{DEFAULT_XML_FILENAME}: tracking master", DEFAULT_XML_FILENAME])
        run_git(["push", git_url])
    except Exception as e:
        logger.warning(e)
        return False

    # Finally create the MR
    merge_requests_url = f"{settings.CI_API_V4_URL}/projects/{settings.CI_PROJECT_ID}/merge_requests"

    failures = "## No failures detected \n\nThe bot compiled every new change and they all worked fine."
    if len(bad_revisions):
        failures = "## Failures \n" \
            "| project | current commit | new commit | logs |\n" \
            "|---------|----------------|------------|------|\n"

        for project, new_revision, tuxsuite_download_url in bad_revisions:
            failures += f"| {project.attrib['name']} | {project.attrib['revision']} | {new_revision} | [build.log]({tuxsuite_download_url}/build.log), [fetch.log]({tuxsuite_download_url}/fetch.log), [bitbake-environment]({tuxsuite_download_url}/bitbake-environment) |\n"

    description = "This merge request was automatically generated by the TRS CI.\n\n" \
        f"{failures}\n\n" \
        "## Note \n" \
        "Each project with newer upstream changes has already been built individually via Tuxsuite.com.\n" \
        f"Please visit {settings.CI_PROJECT_URL}/-/jobs/{settings.CI_JOB_ID} for more details."

    # Ref: https://docs.gitlab.com/ee/api/merge_requests.html#create-mr
    merge_request_spec = {
        "id": settings.CI_PROJECT_ID,
        "source_branch": branch_name,
        "target_branch": "main",
        "remove_source_branch": True,
        "title": f"Automatic updates on {DEFAULT_XML_FILENAME}",
        "allow_maintainer_to_push": True,
        "description": description,
    }

    if settings.TRS_MANIFEST_MR_ASSIGNEE_IDS:
        assignee_ids = settings.TRS_MANIFEST_MR_ASSIGNEE_IDS.split(",")
        merge_request_spec["assignee_ids"] = assignee_ids

    headers = {
        "PRIVATE-TOKEN": settings.TRS_MANIFEST_BOT_USER_TOKEN,
        "Content-Type": "application/json",
    }

    logger.info(f"Opening a merge request via {merge_requests_url}")
    response = requests.post(merge_requests_url, json=merge_request_spec, headers=headers)
    merge_request_id = response.json().get("iid")
    if merge_request_id is None:
        logger.warning(f"Failed to open a merge request: {response.text}")
        return False

    logger.info(f"See merge request: {settings.CI_PROJECT_URL}/-/merge_requests/{merge_request_id}")
    return True


#
#   Build implementation
#
def tip(settings):
    global __settings
    """
        The name tip means always stay on the tip of each repository

        1. Take in default.xml
        2. For each project, get revision and upstream
          2.1. git diff upstream - revision
          2.2. if there are no differences, leave revision as is
          2.3. if there are differences
            2.3.1. trigger a build with the newest revision possible
            2.3.2. if build fails, stop and leave revision as is (for now, as we'll think of new actions later on)
            2.3.3. if build succeeds, add this commit to a list `good_commits`
          2.4. go to the next project
        3. do one last build applying all `good_commits`
        4. if build fails, bummer! Send a notification
        5. if build succeeds, create a commit with auto-merge on, so if it passes both builds
    """

    if not settings.IS_TRS_MANIFEST_REPO:
        logger.warning(f"This script is only supposed to run on {settings.TRS_MANIFEST_URL}! Aborting")
        return False

    if not os.path.exists(DEFAULT_XML_FILENAME):
        logger.warning(f"\"{DEFAULT_XML_FILENAME}\" is missing! Aborting")
        return False

    __settings = settings

    # TODO: check if there isn't a merge request already opened with the same changes

    default_xml = ET.parse(DEFAULT_XML_FILENAME)
    root = default_xml.getroot()
    projects = {p.attrib["name"].replace(".git", "").lower(): p for p in root.findall("project")}
    remotes = {r.attrib["name"]: r for r in root.findall("remote")}
    default_remote = root.findall("default")[0]
    default_remote_name = default_remote.attrib["remote"]
    remotes[None] = remotes[default_remote_name]

    # Check for changes in 4 parallel threads
    inputs = [(p, remotes) for _, p in projects.items()]
    with Pool(4) as p:
        results = p.map(check_project_changes, inputs)

    # Filter out projects with no new revisions
    new_revisions = [r for r in results if r[1] not in [None, False]]
    if len(new_revisions) == 0:
        logger.info("No new revisions detected, work here is done!")
        return True

    # Trigger individual builds on projects with newer revisions
    logger.info(f"Triggering {len(new_revisions)} parallel builds, one for each project with new revision detected")
    with Pool(len(new_revisions)) as p:
        results = p.map(build_individual_project, new_revisions)

    # TODO: use Joakim's bisect idea to try to extract the good commit from failed builds

    # Move all good builds to a list
    good_revisions = [(r[0], r[1]) for r in results if r[3]]
    if len(good_revisions) == 0:
        logger.info("No revisions generated good builds, logs should be revised! Aborting")
        return True

    bad_revisions = [(r[0], r[1], r[2]) for r in results if not r[3]]

    # Create MR changing urls in all good commits
    return create_manifest_mr(settings, good_revisions, bad_revisions, default_xml)


def main():
    settings = Settings(extra=required_vars)
    if settings.missing:
        return False

    return tip(settings)


if __name__ == "__main__":
    sys.exit(0 if main() else 1)
